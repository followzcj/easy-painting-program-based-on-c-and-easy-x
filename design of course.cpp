#include<stdio.h>
#include<graphics.h>
#include<math.h>
#include<windows.h>
int main()
{
	int x, y;
	char a[] = {"颜"}, b[]={"色"}, c[]={"工"}, d[]={"具"},
		e[]={"画笔"}, f[]={"椭圆"}, g[]={"圆形"}, h[]={"矩形"}
	,dl[]={"直线"},cl[]={"擦除"},fl[]={"填充"},ts[]={"喷漆"},clr[]={"清屏"};
	
	initgraph(1200, 750);
	outtextxy(10, 10, a);
	outtextxy(10, 30, b);
	setfillcolor(RED);
	solidrectangle(220, 10, 260, 40);
	setfillcolor(GREEN);
	solidrectangle(100, 10, 140, 40);
	setfillcolor(BLUE);
	solidrectangle(160, 10, 200, 40);
	setfillcolor(WHITE);
	solidrectangle(40, 10, 80, 40);
	
	outtextxy(300, 10, c);
	outtextxy(300, 30, d);
	rectangle(330, 10, 390, 40);
	outtextxy(344, 17, e);
	rectangle(410, 10, 470, 40);
	outtextxy(424, 17, f);
	rectangle(490, 10, 550, 40);
	outtextxy(504, 17, g);
	rectangle(570, 10, 630, 40);
	outtextxy(584, 17, h);
	rectangle(650, 10, 710, 40);
	outtextxy(664, 17, dl);
	rectangle(730, 10, 790, 40);
	outtextxy(744, 17, cl);
	rectangle(810, 10, 870, 40);
	outtextxy(824, 17, fl);
	rectangle(890, 10, 950, 40);
	outtextxy(904, 17, ts);
	rectangle(970, 10, 1030, 40);
	outtextxy(984, 17, clr);
	
	setcolor(BROWN);
	line(0, 48, 1200, 48);//以上都是设置功能选择区域
	
	setcolor(WHITE);//基本色
	MOUSEMSG mmsg;
	int flag = 0, i = 0, j = 0, k = 0;
	int x1 = 0, x2 = 0, y1 = 0, y2 = 0, x3 = 0, y3 = 0;
	while (1)//循环获取鼠标信息
	{
		mmsg = GetMouseMsg();
		x = mmsg.x, y = mmsg.y;	
		if (mmsg.uMsg == WM_LBUTTONDOWN)
		{
			x = mmsg.x, y = mmsg.y;
			if (x <= 80 && x >= 40 && y <= 40 && y >= 10)
				i = 1;
			else if (x >= 100 && x <= 140 && y <= 40 && y >= 10)
				i = 2;
			else if (x >= 160 && x <= 200 && y <= 40 && y >= 10)
				i = 3;
			else if (x >= 220 && x <= 260 && y <= 40 && y >= 10)
				i = 4;
			else if (x >= 330 && x <= 390 && y <= 40 && y >= 10)
				j = 1;
			else if (x >= 410 && x <= 470 && y <= 40 && y >= 10)
				j = 2;
			else if (x >= 490 && x <= 550 && y <= 40 && y >= 10)
				j = 3;
			else if (x >= 570 && x <= 630 && y <= 40 && y >= 10)
				j = 4;
			else if (x >= 650 && x <= 710 && y <= 40 && y >= 10)
				j = 5;
			else if (x >= 730 && x <= 790 && y <= 40 && y >= 10)
				j = 6;
			else if (x >= 810 && x <= 870 && y <= 40 && y >= 10)
				j = 7;
			else if (x >= 890 && x <= 950 && y <= 40 && y >= 10)
				j = 8;
			else if (x >= 970 && x <= 1030 && y <= 40 && y >= 10)
				j = 9;//为每次点击功能区设置flag
		}
		switch (i)//switch语句分条件相应点击事件
		{
		case 1: setcolor(WHITE); break;
		case 2: setcolor(GREEN); break;
		case 3: setcolor(BLUE); break;
		case 4: setcolor(RED); break;
		default:break;
		}
		if (j == 1 && mmsg.y >= 50)//实现画笔功能
		{
			flag = 0;
			while (1)
			{
				mmsg = GetMouseMsg();
				x = mmsg.x, y = mmsg.y;
				switch (i)
				{
				case 1: setcolor(WHITE); break;
				case 2: setcolor(GREEN); break;
				case 3: setcolor(BLUE); break;
				case 4: setcolor(RED); break;
				default:break;
				}
				if (mmsg.y >= 0 && mmsg.y < 50)
				{
					break;
				}
				if (mmsg.uMsg == WM_LBUTTONDOWN)
				{
					if (flag == 0)
					{
						x1 = mmsg.x;
						y1 = mmsg.y;
						flag = 1;
					}
				}
				else if (mmsg.uMsg == WM_MOUSEMOVE && flag == 1)
				{
					x2 = mmsg.x;
					y2 = mmsg.y;
					line((int)x1, (int)y1, (int)x2, (int)y2);
					x1 = x2;
					y1 = y2;
				}
				if (mmsg.uMsg == WM_LBUTTONUP)
					flag = 0;
			}
		}
		else if (j == 2 && mmsg.y >= 50)//实现画椭圆功能（仍然有瑕疵，但暂时做到这个地步），因为graphics头文件中画椭圆的形式比较特别，输入的实参影响画出的圆，瑕疵椭圆在报告中详述
		{
			setcolor(BLACK);
			while (1)
			{
				mmsg = GetMouseMsg();
				if (mmsg.uMsg == WM_LBUTTONDOWN)
				{
					if (flag == 0)
					{
						x1 = mmsg.x;
						y1 = mmsg.y;
						x3 = x1;
						y3 = y1;
						flag = 1;
					}
				}
				else if (mmsg.uMsg == WM_MOUSEMOVE && flag == 1)
				{
					x2 = mmsg.x;
					y2 = mmsg.y;
				}
				if (mmsg.uMsg == WM_LBUTTONUP && flag == 1)
				{
					flag = 0;
					continue;
				}
				if (mmsg.y >= 0 && mmsg.y < 50)
				{
					break;
				}
				if (mmsg.uMsg == WM_MOUSEMOVE && flag == 1)
				{	
					switch (i)
					{
					case 1: setfillcolor(WHITE); break;
					case 2: setfillcolor(GREEN); break;
					case 3: setfillcolor(BLUE); break;
					case 4: setfillcolor(RED); break;
					default:break;
					}
					fillellipse(x3, y3, x2, y2);
				}
			}
			
		}
		else if (j == 3 && mmsg.y >= 50)//实现画圆功能
		{
			flag = 0;
			while (1)
			{
				mmsg = GetMouseMsg();
				x = mmsg.x, y = mmsg.y;
				switch (i)
				{
				case 1: setfillcolor(WHITE); break;
				case 2: setfillcolor(GREEN); break;
				case 3: setfillcolor(BLUE); break;
				case 4: setfillcolor(RED); break;
				default:break;
				}
				if (mmsg.y >= 0 && mmsg.y < 50)
				{
					break;
				}
				if (mmsg.uMsg == WM_LBUTTONDOWN)
				{
					if (flag == 0)
					{
						x1 = mmsg.x;
						y1 = mmsg.y;
						x3 = x1;
						y3 = y1;
						flag = 1;
					}
				}
				else if (mmsg.uMsg == WM_MOUSEMOVE && flag == 1)
				{
					x2 = mmsg.x;
					y2 = mmsg.y;
				}
				if (mmsg.uMsg == WM_LBUTTONUP && flag == 1)
				{
					flag = 0;
					continue;
				}
				
				if (mmsg.uMsg == WM_MOUSEMOVE && flag == 1)
				{
					if ((y1 + y3) / 2 - sqrt((x1 - x2)*(x1 - x2) + (y1 - y3)*(y1 - y3)) / 2 <= 50)
					{
						flag = 0;
						
						fillcircle((x1 + x3) / 2, (y1 + y3) / 2, sqrt((x1 - x3)*(x1 - x3) + (y1 - y3)*(y1 - y3)) / 2);
						continue;
					}
					fillcircle((x1 + x2) / 2, (y1 + y2) / 2, sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2)) / 2);
					x3 = x2, y3 = y2;
				}
			}
		}
		else if (j == 4 && mmsg.y >= 50)//实现画矩形和正方形功能，因为要根据鼠标移动事件来画矩形，不方便实时显示正方形，因此融合，本身能力也有限……o(╯□╰)o
		{
			flag = 0;
			while (1)
			{
				mmsg = GetMouseMsg();
				x = mmsg.x, y = mmsg.y;
				switch (i)
				{
				case 1: setfillcolor(WHITE); break;
				case 2: setfillcolor(GREEN); break;
				case 3: setfillcolor(BLUE); break;
				case 4: setfillcolor(RED); break;
				default:break;
				}
				if (mmsg.y >= 0 && mmsg.y < 50)
				{
					break;
				}
				if (mmsg.uMsg == WM_LBUTTONDOWN)
				{
					if (flag == 0)
					{
						x1 = mmsg.x;
						y1 = mmsg.y;
						flag = 1;
						x3 = x1;
						y3 = y1;
					}
				}
				if (mmsg.uMsg == WM_MOUSEMOVE && flag == 1)
				{
					x2 = mmsg.x;
					y2 = mmsg.y;
				}
				if (mmsg.uMsg == WM_LBUTTONUP && flag == 1)
				{
					flag = 0;
					continue;
				}
				if (mmsg.uMsg == WM_MOUSEMOVE && flag == 1)
				{
					fillrectangle(x1, y1, x2, y2);
					y3 = y2;
					x3 = x2;
				}
			}
		}
		else if (j == 5 && mmsg.y >= 50)//实现画直线功能
		{
			while (1)
			{
				mmsg = GetMouseMsg();
				x = mmsg.x, y = mmsg.y;
				switch (i)
				{
				case 1: setcolor(WHITE); break;
				case 2: setcolor(GREEN); break;
				case 3: setcolor(BLUE); break;
				case 4: setcolor(RED); break;
				default:break;
				}
				if (mmsg.y >= 0 && mmsg.y < 50)
				{
					break;
				}
				if (mmsg.uMsg == WM_LBUTTONDOWN)
				{
					if (flag == 0)
					{
						x1 = mmsg.x;
						y1 = mmsg.y;
						flag = 1;
					}
					else if (flag == 1)
					{
						x2 = mmsg.x;
						y2 = mmsg.y;
						line((int)x1, (int)y1, (int)x2, (int)y2);
						x1 = x2;
						y1 = y2;
						j = 1;
						break;
					}
				}
			}
		}
		else if (j == 6 && mmsg.y >= 50)//实现圆形橡皮擦功能
		{
			flag = 0;
			while(1)
			{
				mmsg = GetMouseMsg();
				if (mmsg.y >= 0 && mmsg.y < 55)
				{
					break;
				}
				if(mmsg.uMsg == WM_LBUTTONDOWN)
				{
					if(flag == 0)
					{
						x1 = mmsg.x;
						y1 = mmsg.y;
						flag = 1;
					}
				}
				if(mmsg.uMsg == WM_LBUTTONUP && flag == 1)
				{
					flag = 0;
					continue;
				}
				if(mmsg.uMsg == WM_MOUSEMOVE && flag == 1)
				{
					x2 = mmsg.x;
					y2 = mmsg.y;
					setfillcolor(BLACK);
					solidcircle(x2,y2,5);
				}
			}
		}
		else if (j == 7 && mmsg.y >= 50)//实现填充功能
		{
			flag = 0;
			while(1)
			{
				mmsg = GetMouseMsg();
				x = mmsg.x;
				y = mmsg.y;
				if (mmsg.y >= 0 && mmsg.y < 50)
				{
					break;
				}
				if(mmsg.uMsg == WM_LBUTTONDOWN)
				{
					if(flag == 0)
					{
						x1 = mmsg.x;
						x2 = mmsg.y;
						flag = 1;
					}
				}
				if(mmsg.uMsg == WM_LBUTTONUP && flag ==1)
				{
					switch (i)
					{
					case 1: setfillcolor(WHITE); break;
					case 2: setfillcolor(GREEN); break;
					case 3: setfillcolor(BLUE); break;
					case 4: setfillcolor(RED); break;
					default:break;
					}
					floodfill(x1,x2,0);
					flag = 0;
					break;
				}
			}
		}
		else if (j == 8 && mmsg.y >= 50)//实现喷漆功能
		{
			flag = 0;
			while(1)
			{
				mmsg = GetMouseMsg();
				if (mmsg.y >= 0 && mmsg.y < 70)
				{
					break;
				}
				if(mmsg.uMsg == WM_LBUTTONDOWN)
				{
					if(flag == 0)
					{
						x1 = mmsg.x;
						y1 = mmsg.y;
						flag = 1;
					}
				}
				if(mmsg.uMsg == WM_LBUTTONUP && flag == 1)
				{
					flag = 0;
					continue;
				}
				if(mmsg.uMsg == WM_MOUSEMOVE && flag == 1)
				{
					x2 = mmsg.x;
					y2 = mmsg.y;
					switch (i)
					{
					case 1: setfillcolor(WHITE); break;
					case 2: setfillcolor(GREEN); break;
					case 3: setfillcolor(BLUE); break;
					case 4: setfillcolor(RED); break;
					default:break;
					}
					solidcircle(x2,y2,20);
				}
			}
		}
		else if (j == 9 && mmsg.y >= 50)//实现清屏功能
		{
					setfillcolor(BLACK);
					solidrectangle(0,51,1200,750);
		}
	}
	
	getchar();
	closegraph();
	return 0;
}
